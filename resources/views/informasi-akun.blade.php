<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Event Online</title>
    <!-- CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/dasbor.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Red+Hat+Display:wght@700&display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Nova+Square&display=swap" rel="stylesheet">
     <!-- Icons -->
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons"  rel="stylesheet">
     <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined"  rel="stylesheet">
    <!-- JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
</head>
<body>
    <div class="navbar">
        <div class="container d-flex align-items-center justify-content-center">
            <div class="row">
                <div class="col-1 d-flex align-items-center">
                    <h3>ONEVE</h3>
                </div>
                <div class="col-2 d-flex align-items-center justify-content-center">
                   <a href="" class="btn cari-event">Cari Event</a>
                </div>
                <div class="col-6">
                    <form action="" class="form">
                        <div class="form-control d-flex align-items-center">
                            <input type="text" placeholder="Search" class="form-search">
                            <button class="icon-search d-flex align-items-center">
                                <i class="material-icons-outlined">
                                    search
                                </i>    
                            </button>                                 
                        </div>
                    </form>
                </div>
                <div class="col-3 d-flex align-items-center justify-content-end">
                    <a href="" class="btn btn-buat">Buat Event</a>
                    <div class="dropdown">
                        <button class="account btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="/src/img/user/Emma-Watson140422.jpg" width="48px" height="48px"/>
                        </button>
                        <div class="dropdown-menu dropdown-menu-end dropdown-menu-right dropdown-account" aria-labelledby="dropdownMenuButton">
                          <div class="d-flex dd-account align-items-center justify-content-center">
                              <div>
                                <img src="src/img/user/Emma-Watson140422.jpg" width="48px" height="48px"/>
                              </div>
                              <p class="nama-user">Fadiah Ahmad</p>
                          </div>
                          <a class="dropdown-item d-flex align-items-center" href="#">
                            <i class="material-icons-outlined">
                                dashboard
                                </i>
                                Dasbor
                            </a>
                          <a class="dropdown-item d-flex align-items-center" href="#">
                            <i class="material-icons">
                                person
                                </i>
                                Profil
                            </a>
                          <a class="dropdown-item d-flex align-items-center" href="#">
                            <i class="material-icons-outlined">
                                logout
                                </i>
                                Keluar
                            </a>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
    
    @if(Session::get('message'))
    <div class="container-alert d-flex justify-content-center">
      <div class="alert alert-primary" role="alert">
        {{ Session::get('message') }}
      </div>
    </div>
  @endif
  @if(Session::get('error'))
  <div class="container-alert d-flex justify-content-center">
    <div class="alert alert-danger" role="alert">
      {{ Session::get('error') }}
    </div>
  </div>
  @endif

    <div class="container-fluid container-dasbor">
        <div class="main-row d-flex">
            <div class="col-3 sidebar d-flex flex-column">
                <div class="profile-sidebar d-flex flex-column align-items-center justify-content-center">
                    
                  <form enctype="multipart/form-data" action="{{route('ppupload')}}" method="POST" class="banner-upload d-flex flex-column justify-content-center align-items-center">
                      <div class="banner-edit">
                          <input type='file' id="imageUpload-pp" accept=".png, .jpg, .jpeg" name="imageUpload" class=" imageUpload-pp" />
                          <input type="hidden" name="base64image" name="base64image" id="base64image">
                      </div>
                      <div class="banner-preview container2 d-flex flex-column justify-content-center">
                          @php
                              if(!empty($image->image) && $image->image!='' && file_exists(public_path('images/'.$image->image))){
                                $image =$image->image;
                              }else{
                                $image = 'default.png';
                              }
                              $url = url('public/images/'.$image);
                              $imgs =  "background-image:url($url)";
                              $norepeat =  "background-repeat: no-repeat";
                              $cover =  "background-size: cover";
                              $radius = "border-radius: 1451px";
                              $sizewidth = "width: 120px";
                              $sizeheight = "height: 120px;";
                                
                          @endphp
                          <div class="container-imagepreview-pp">
                              <div id="imagePreview-pp" style="{{$imgs}}; {{$norepeat}}; {{$cover}}; {{$radius}}; {{$sizewidth}}; {{$sizeheight}};">
                                  <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                                  <label class="btn button-action" for="imageUpload-pp">
                                    <i class="material-icons">
                                      edit
                                  </i>  
                                  </label>
                                  
                              </div>
                          </div>
                      </div>
                      {{-- <button type="submit">ss</button> --}}
                  

                  {{-- Modal Upload PP --}}
                  <div class="modal fade bd-example-modal-lg imagecrop" id="model" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                    <div class="modal-dialog modal-dialog-centered lg d-flex flex-column align-items-center justify-content-center">
                        <div class="modal-content modal-upload-pp">
                            <div class="modal-header d-flex flex-column align-items-end">
                                <button type="button" class="close-pp" data-dismiss="modal" aria-label="Close">
                                  <i class="material-icons-outlined">
                                    close
                                    </i>
                                </button>
                            </div>
                            <div class="row d-flex justify-content-center">
                                <h4>UPLOAD FOTO PROFIL</h4>
                            </div>
                            <div class="modal-body">
                                <p>Sesuaikan Gambar Sesuai Keinginan</p>
                                <div class="row">
                                    <div class="col-md-12 d-flex flex-column align-items-center justify-content-center">
                                        <img id="image">
                                    </div>
                                </div>
                                <div class="row row-footer d-flex">
                                    <div class="col-6 d-flex justify-content-start">
                                        <button type="button" class="btn btn-secondary crop-close" data-dismiss="modal">Kembali</button>
                                    </div>
                                    <div class="col-6 d-flex justify-content-end">   
                                        <button type="button" class="btn btn-primary crop" id="crop">Simpan</button>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>

                    <h4 class="nama-user-sidebar">Fadiah Ahmad</h4>
                </div>  
                <hr>
                <div class="sidebar-button-container d-flex flex-column">
                    <p class="sidebar-title">Dasbor</p>
                <a href="dashbor - beranda.html" class="sidebar-button d-flex align-items-center">
                    <svg class="rumah" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M4 12V21.3651C4 21.5335 4.07662 21.695 4.21301 21.814C4.3494 21.9331 4.53439 22 4.72727 22H9.09091V16.6032C9.09091 16.3506 9.20584 16.1083 9.41043 15.9297C9.61501 15.7511 9.89249 15.6508 10.1818 15.6508H13.8182C14.1075 15.6508 14.385 15.7511 14.5896 15.9297C14.7942 16.1083 14.9091 16.3506 14.9091 16.6032V22H19.2727C19.4656 22 19.6506 21.9331 19.787 21.814C19.9234 21.695 20 21.5335 20 21.3651V12" stroke="#797979" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M18.75 8.39053V2.99991H16.5V6.23428M22.5 11.9999L12.5105 2.43741C12.2761 2.18991 11.7281 2.1871 11.4895 2.43741L1.5 11.9999H22.5Z" stroke="#797979" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>      
                    Beranda                   
                </a>
                <a href="event-saya-berlangsung.html" class="sidebar-button hover d-flex align-items-center">
                    <i class="material-icons-outlined">
                        event
                    </i>
                    Event Saya
                </a>
                <a href="tiket-saya-berlangsung.html" class="sidebar-button d-flex align-items-center">
                    <ion-icon name="ticket-outline"></ion-icon>     
                    Tiket Event Dibeli
                </a>
                </div>
                
                <div class="sidebar-button-container d-flex flex-column">
                    <p class="sidebar-title">Dasbor Event</p>
                    <a href="data-pemesanan.html" class="sidebar-button d-flex align-items-center">
                        <i class="material-icons-outlined">
                            people
                            </i>   
                        Data Pemesanan                
                    </a>
                </div>

                <div class="sidebar-button-container d-flex flex-column" >
                    <p class="sidebar-title">Profil</p>
                    <a href="informasi-akun.html" class="sidebar-button d-flex align-items-center sidebar-active">
                        <i class="material-icons-outlined">
                            account_circle
                            </i> 
                       Informasi Akun      
                    </a>
                    <a href="favorit-event-berlangsung.html" class="sidebar-button d-flex align-items-center">
                        <i class="material-icons-outlined">
                            favorite
                            </i>   
                        Favorit event          
                    </a>
                </div>
            </div>
            <div class="col-9 container-cards">
                <div class="row">
                  <div action="" class="informasi-akun" id="form">
                    <h4>INFORMASI AKUN</h4>
                    <div class="form-group">
                      <label for="exampleFormControlInput1">Nama</label>
                      <div class="input-informasi-akun d-flex align-items-center">
                        <input type="text" class="form-control form-informasi-akun" id="nama">
                        <i class="material-icons-outlined">
                          error_outline
                          </i>
                      </div>     
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlInput1">Email</label>
                      <div class="input-informasi-akun d-flex align-items-center">
                        <input type="email" class="form-control form-informasi-akun" id="email">
                        <i class="material-icons-outlined">
                          error_outline
                          </i>
                      </div>   
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlInput1">Nomor Telepon</label>
                      <div class="input-informasi-akun d-flex align-items-center">
                        <input type="text" class="form-control form-informasi-akun" id="telepon">
                         <i class="material-icons-outlined">
                          error_outline
                          </i>
                      </div>   
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlInput1">NIK</label>
                      <div class="input-informasi-akun d-flex align-items-center">
                        <input type="text" class="form-control form-informasi-akun" id="nik">
                         <i class="material-icons-outlined">
                          error_outline
                          </i>
                      </div>   
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlInput1">Nama Instansi</label>
                      <div class="input-informasi-akun d-flex align-items-center">
                        <input type="text" class="form-control form-informasi-akun" id="instansi">
                         <i class="material-icons-outlined">
                          error_outline
                          </i>
                      </div>   
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlInput1">Nama Penanggung Jawab</label>
                      <div class="input-informasi-akun d-flex align-items-center">
                        <input type="text" class="form-control form-informasi-akun" id="penanggungJawab">
                         <i class="material-icons-outlined">
                          error_outline
                          </i>
                      </div>   
                    </div>
                    <div class="form">
                      <label for="">Foto KTP</label>
                      <div class="grid">
                        <div class="form-element">
                          <input type="file" id="file-1" accept="image/png, image/jpeg">
                          <label for="file-1" id="file-1-preview">
                            <img src="https://www.pngkit.com/png/full/9-98686_blank-png.png" style="border-radius: 8px" alt="">
                            <div>
                              <span>+</span>
                            </div>
                          </label>
                          
                        </div>
                        <label  for="file-1"  class="overlay d-flex flex-column align-items-center justify-content-center">
                          <i class="material-icons">add_circle_outline</i>
                          <p>Upload Foto KTP</p>
                        </label>
                      </div>
                    </div>
                    <div class="form-group d-flex justify-content-end">
                      <button type="submit" class="btn btn-simpan-informasi">Simpan</button>
                    </div>
                  </div>
                  </form>
                </div>
            </div>
        </div>
    </div>

    <footer>
      <div class="container">
        <div class="row">
          <div class="col d-flex align-items-center justify-content-end">
            Copyright ©2021
          </div>
        </div>
      </div>
    </footer>
    
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
    <script>
      var $modal = $('.imagecrop');
      var image = document.getElementById('image');
      var cropper;

      $("body").on("change", ".imageUpload-pp", function(e){
          var files = e.target.files;
          var done = function(url) {
              image.src = url;
              $modal.modal('show');
          };
          var reader;
          var file;
          var url;
          if (files && files.length > 0) {
              file = files[0];
              if (URL) {
                  done(URL.createObjectURL(file));
              } else if (FileReader) {
                  reader = new FileReader();
                  reader.onload = function(e) {
                      done(reader.result);
                  };
                  reader.readAsDataURL(file);
              }
          }
      });
      $modal.on('shown.bs.modal', function() {
          cropper = new Cropper(image, {
              // aspectRatio: 17/10,
              viewMode: 1,
              dragMode: 'move',
              cropBoxResizable: false,
              data:{ //define cropbox size
                width: 120,
                height:  120,
              },
          });
      }).on('hidden.bs.modal', function() {
          cropper.destroy();
          cropper = null;
      });
      $("body").on("click", "#crop", function() {
          canvas = cropper.getCroppedCanvas({
              width: 120,
              height: 120,
          });
          canvas.toBlob(function(blob) {
              url = URL.createObjectURL(blob);
              var reader = new FileReader();
              reader.readAsDataURL(blob);
              reader.onloadend = function() {
                   var base64data = reader.result;
                   $('#base64image').val(base64data);
                   document.getElementById('imagePreview-pp').style.backgroundImage = "url("+base64data+")";
                   $modal.modal('hide');
              }
          });
      })

</script>
<script>
function disabledTb(){
  var bayar = document.getElementById('ticket-priceTypePaid');
  var formHarga = document.getElementById('ticket-price');
  formHarga.disabled = bayar.checked ? false : true;
  formHarga.value="";
  if(!formHarga.disabled){
      formHarga.focus();
  }
}
function enabledTB(){
  var gratisan = document.getElementById('ticket-priceTypeFree');
  var formHarga = document.getElementById('ticket-price');
  formHarga.disabled = gratisan.checked ? false : true;
  formHarga.value="";
  if(!formHarga.focus){
      formHarga.disabled;
  }
}
</script>
<script>
  // $('#event-startDate').datepicker({
  //     minDate:0
  // });
  // $('#event-endDate').datepicker({
  //     minDate: "datepickerstart"
  // });

  $(document).ready(function(){
  $("#event-startDate").datepicker({
      minDate:0,
      numberOfMonths: 1,
      onSelect: function(selected) {
        $("#event-endDate").datepicker("option","minDate", selected)
      }
  });
  $("#event-endDate").datepicker({ 
      minDate:0,
      numberOfMonths: 1,
      onSelect: function(selected) {
         $("#event-startDate").datepicker("option","maxDate", selected)
      }
  });  
});
  
  
</script>
<script>
  $(document).ready(function(){
  $("#ticket-startDate").datepicker({
      minDate:0,
      numberOfMonths: 1,
      onSelect: function(selected) {
        $("#ticket-endDate").datepicker("option","minDate", selected)
      }
  });
  $("#ticket-endDate").datepicker({ 
      minDate:0,
      numberOfMonths: 1,
      dateFormat: 'd MM yy',
      onSelect: function(selected) {
         $("#ticket-startDate").datepicker("option","maxDate", selected)
      }
  });  
});
</script>
<script>
initSample();
</script>
<script>
  var dateStart = $('#event-startDate').datepicker({ 
      minDate:0,
      numberOfMonths: 1,
      dateFormat: 'd MM yy',
      monthNames: [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ],
      onSelect: function(selected) {
        $("#event-endDate").datepicker("option","minDate", selected)
      }
   }).val();

   var dateEnd = $('#event-endDate').datepicker({ 
      minDate:0,
      numberOfMonths: 1,
      dateFormat: 'd MM yy',
      monthNames: [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ],
      onSelect: function(selected) {
        $("#event-startDate").datepicker("option","maxDate", selected)
      }
   }).val();
   
  // $(document).ready(function(){
  //     $('#event-startDate').datepicker({
  //         dateFormat: "d-"
  //     });
  // });

  
</script>
<script>
  var tiketdateStart = $('#ticket-startDate').datepicker({ 
      minDate:0,
      numberOfMonths: 1,
      dateFormat: 'd MM yy',
      monthNames: [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ],
      onSelect: function(selected) {
        $("#ticket-endDate").datepicker("option","minDate", selected)
      }
   }).val();

   var tiketdateEnd = $('#ticket-endDate').datepicker({ 
      minDate:0,
      numberOfMonths: 1,
      dateFormat: 'd MM yy',
      monthNames: [ "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" ],
      onSelect: function(selected) {
        $("#ticket-startDate").datepicker("option","maxDate", selected)
      }
   }).val();
</script>
    <script>
      function previewBeforeUpload(id){
        document.querySelector("#"+id).addEventListener("change",function(e){
          if(e.target.files.length == 0){
            return;
          }
          let file = e.target.files[0];
          let url = URL.createObjectURL(file);
          document.querySelector("#"+id+"-preview div").innerText = file.name;
          document.querySelector("#"+id+"-preview img").src = url;
        });
      }

      previewBeforeUpload("file-1");
      previewBeforeUpload("file-2");
      previewBeforeUpload("file-3");
    </script>
    <script>
      const form = document.getElementById('form');
      const nama = document.getElementById('nama');
      const email = document.getElementById('email');
      const telepon = document.getElementById('telepon');
      const nik = document.getElementById('nik');
      const instansi = document.getElementById('instansi');
      const penanggungJawab = document.getElementById('penanggungJawab');

      form.addEventListener('submit', (e) => {
        e.preventDefault();

        checkInputs();
      });

      function checkInputs() {
        // Value dari input
        const namaValue = nama.value;
        const emailValue = email.value.trim();
        const teleponValue = telepon.value.trim();
        const nikValue = nik.value.trim();
        const instansiValue = instansi.value.trim();
        const penanggungValue = penanggungJawab.value.trim();

        if(namaValue === '' ) {
          // error msg
          setErrorFor(nama);
        }else{
          setSuccessFor(nama);
        }

        if(emailValue === '' ) {
          // error msg
          setErrorFor(email);
        }else{
          setSuccessFor(email);
        }

        if(teleponValue === '' ) {
          // error msg
          setErrorFor(telepon);
        }else{
          setSuccessFor(telepon);
        }

        if(nikValue === '' ) {
          // error msg
          setErrorFor(nik);
        }else{
          setSuccessFor(nik);
        }

        if(instansiValue === '' ) {
          // error msg
          setErrorFor(instansi);
        }else{
          setSuccessFor(instansi);
        }

        if(penanggungValue === '' ) {
          // error msg
          setErrorFor(penanggungJawab);
        }else{
          setSuccessFor(penanggungJawab);
        }
      }
      function setErrorFor(input){
        const inputInformasi = input.parentElement;

        inputInformasi.className = 'input-informasi-akun error';
      }

      function setSuccessFor(input){
        const inputInformasi = input.parentElement;

        inputInformasi.className = 'input-informasi-akun success';
      }
    </script>
</body>
</html>