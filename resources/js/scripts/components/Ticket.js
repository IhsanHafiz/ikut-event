import HTMLHelper from '../utils/HTMLHelper.js';
const { toElement } = HTMLHelper;


// TICKET COMPONENTS
const Ticket = ({
  eventName = ' ', 
  endDate   = ' ',
  startTime = ' ',
  endTime   = ' ',
  price     = 0,
  stocks    = 0
} = {}) => (
  toElement(`
    <div class="tiket"> 
      <div class="row row-keterangan-tiket">
        ${FirstSection({eventName, endDate, startTime, endTime})}
        ${SecondSection({price, stocks})}
      </div>
    </div>
  `)
);


// FIRST SECTION
const FirstSection = ({eventName, endDate, startTime, endTime}) => (`
  <div class="col-6">
    <div class="row">
      <div class="col-12 judul-tiket">
        <h3>${eventName}</h3>
      </div>
      <div class="col-12 ticket-ends">
        <div class="row">
          <div class="col-7">
            <p>Berakhir pada ${endDate}</p>
          </div>
          <div class="col-5">
            <p>${startTime} - ${endTime}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
`);


// SECOND SECTION
const SecondSection = ({price, stocks}) => (`
  <div class="col-6 d-flex align-items-center">
    <div class="col-6 harga-tiket d-flex justify-content-end">
      Rp ${price || 0}
    </div>
    <div class="col-6 action d-flex justify-content-end">
    <a class="btn btn-primary d-flex justify-content-center button-action edit-ticket"  data-toggle="modal" data-target="#edit-detail-tiket">
      <i class="material-icons edit">
      edit
      </i>
    </a> 
      <button class="button-action delete-ticket" type="button">
        <i class="material-icons edit">
          delete
        </i>
      </button>
    </div>
  </div>
`);


export default Ticket;